# Flask-RESTful
Flask-RESTful provides the building blocks for creating a great REST API.

# Installation:
- python --version
- pip3 install Flask-RESTful
- pip3 install Flask-JWT

# Resources:
You'll find the user guide and all documentation below:
1. https://flask-restful.readthedocs.io/en/latest/

2. https://codehandbook.org/flask-restful-api-using-python-mysql/

